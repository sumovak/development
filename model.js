class Model {
    constructor() {
        // pole objektu Poznamka
        this._poznamky = [];
        if(!localStorage.getItem("vse")) {
            localStorage.setItem("vse",JSON.stringify([]));
        }
    }


    ulozPozn(text) {
        if(text.length > 0){
            var pozn = new Poznamka(text);
            this.pridejDoLS(pozn.getJSON());
        }else{
            uklidError();
        }
    }

    pridejDoLS(hodnota) {
        var vse = JSON.parse(localStorage.getItem("vse"));
        if (vse == null) vse = [];
        vse.push(hodnota);
        localStorage.setItem("vse", JSON.stringify(vse));
    }

    ziskejVse() {
        this._poznamky = [];
        let ulozene = JSON.parse(localStorage.getItem("vse"));
        for (let i = 0; i < ulozene.length; i++) {
            this._poznamky.push(new Poznamka(ulozene[i].text));
        }
    }
    vypisPoznamky() {
        let pocetR = Math.ceil(this._poznamky.length / 5);
        let pocetP = Math.ceil(this._poznamky.length / pocetR);
        let index = 0;
        const row = document.createElement("div");
        row.className = "flexWrap";
        for(const p of this._poznamky) {

            const pozEle = (p.getHtml(index));
            pozEle.onclick = function () {
                this.parentElement.removeChild(this);
                model.vymazPoznamku(this.id);
                //napojuje se na globální proměnnou z kontroler
                console.log(index);
            }
            index++;
            row.appendChild(pozEle);
        }
        let postits = document.getElementById("postit");
        postits.innerHTML="";
        postits.appendChild(row);
    }
vymazPoznamku(index){
    let novy = [];
    this._poznamky.splice(index,1);
    console.log('Poznamky:',this._poznamky);
    for(const p of this._poznamky) {
        novy.push(p.getJSON());
    }
    localStorage.clear();
    localStorage.setItem("vse", JSON.stringify(novy));
    this.ziskejVse();
    this.vypisPoznamky();
 }
vymazVse() {
    let novy=[];
    window.localStorage.clear();
    localStorage.setItem("vse", JSON.stringify(novy));
    this.ziskejVse();
}
}


